# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
"""
Created on Tue Apr 26 15:12:49 2022

@author: jelmer
"""
import numpy as np
import matplotlib.pyplot as plt
from fastpsf import Context, GaussianPSFMethods, CSplineMethods, CSplineCalibration, Gauss3D_Calibration


gauss3D_calib = Gauss3D_Calibration(
    [1.0, 0.19020476937294006, 0.15444783866405487, 0.10000000149011612],
    [1.0, -0.1928471326828003, 0.15837538242340088, 9.999999747378752e-06])

roisize = 20

with Context(debugMode=False) as ctx:
    m = GaussianPSFMethods(ctx)
    
    params = np.zeros((60,5))
    params[:,:2] = roisize/2
    params[:,2] = np.linspace(-0.5,0.5,len(params))
    params[:,3] = 1
    
    psf = m.CreatePSF_XYZIBg(roisize, gauss3D_calib, True)
    zstack = psf.ExpectedValue(params) 

    zrange = [params[0,2], params[-1,2]]
    cm = CSplineMethods(ctx)
    cspsf = cm.CreatePSFFromZStack(roisize, zstack, zrange, cuda=False)

    p2 = params[[0]]*1
    p2[:,:2] = roisize/2
    ev = cspsf.ExpectedValue(p2)
    
    #cspsf2 = cm.CreatePSF_XYZIBg(roisize, cspsf.calib, fitMode=CSplineMethods.FixedIntensityAndBg, cuda=False)
    cspsf2 = cm.CreatePSFFromZStack(roisize, zstack, zrange, fitMode=CSplineMethods.FixedIntensityAndBg, cuda=False)
    
    ev2 = cspsf2.ExpectedValue(p2[:,:3], constants = p2[:,3:])

    cspsf3 = cm.CreatePSFFromZStack(roisize, zstack, zrange, fitMode=CSplineMethods.FixedBg, cuda=False)
    ev3 = cspsf3.ExpectedValue(p2[:,:4], constants = p2[:,4:])

    i = 0
    fig,ax=plt.subplots(2,2)
    ax[0,0].imshow(zstack[i])
    ax[0,1].imshow(ev[i])
    ax[1,0].imshow(ev2[i])
    ax[1,1].imshow(ev3[i])

