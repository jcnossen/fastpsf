# -*- coding: utf-8 -*-
"""
Created on Tue Apr 26 15:12:49 2022

@author: jelmer
"""

import matplotlib.pyplot as plt
from fastpsf import Context, GaussianPSFMethods

with Context() as ctx:
    m = GaussianPSFMethods(ctx)
    
    psf = m.CreatePSF_XYIBg(10, 1.3, cuda=True)
    
    params = [[4,5,300,2]] 
    smp = psf.GenerateSample(params)
    plt.imshow(smp[0],cmap='gray')
    plt.title('Sample of fluorescent emitter')
    
    print(psf.CRLB(params))
    
        
    