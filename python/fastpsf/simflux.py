# -*- coding: utf-8 -*-


import ctypes
import numpy as np

import numpy.ctypeslib as ctl

from . import gaussian
from .context import Context
from .estimator import Estimator

Theta = ctypes.c_float * 4
FisherMatrix = ctypes.c_float * 16
Modulation = ctypes.c_float * 4




class SIMFLUX:
    
    modulationDType = np.dtype([('k', '<f4', (3,)), 
                                ('depth','<f4'),
                                ('phase','<f4'),
                                ('relint','<f4')
                                ])
    
    def __init__(self, ctx:Context):
        self.ctx = ctx
        lib = ctx.lib.lib
        
        
#CDLL_EXPORT PSF* SIMFLUX2D_Gauss2D_PSF_Create(SIMFLUX_Modulation* mod, int num_patterns, 
# float sigma, int roisize, int numframes, bool simfluxMode, Context* ctx);
        
        self._SIMFLUX_Gauss2D_CreateEstimator = lib.SIMFLUX_Gauss2D_CreateEstimator
        self._SIMFLUX_Gauss2D_CreateEstimator.argtypes= [
                ctypes.c_int, # numpatterns
                ctypes.c_float, # sigma_x
                ctypes.c_float, # sigma_y
                ctypes.c_int, # roisize
                ctypes.c_int, # nframes
                ctypes.c_void_p # context
                ]
        self._SIMFLUX_Gauss2D_CreateEstimator.restype = ctypes.c_void_p


        
        self._SIMFLUX_Gauss2DAstig_CreateEstimator = lib.SIMFLUX_Gauss2DAstig_CreateEstimator
        self._SIMFLUX_Gauss2DAstig_CreateEstimator.argtypes= [
                ctypes.c_int, # numpatterns
                ctypes.POINTER(gaussian.Gauss3D_Calibration), 
                ctypes.c_int, # roisize
                ctypes.c_int, # nframes
                ctypes.c_void_p # context
                ]
        self._SIMFLUX_Gauss2DAstig_CreateEstimator.restype = ctypes.c_void_p

        self._DFT2D = lib.DFT2D
        self._DFT2D.argtypes = [
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"),  # xyI
            ctypes.c_int32,  # numpts
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"),  # k
            ctypes.c_int32,  # numk
            ctl.ndpointer(np.complex64, flags="aligned, c_contiguous"),  # output
            ctypes.c_bool # useCuda
        ]

    
    def CreateEstimator_Gauss2D(self, calib, num_patterns, roisize, 
                                    numframes, simfluxEstim=True) -> Estimator:
        """
        Each spot requires 6 * numPatterns constants describing the modulation patterns:
        a matrix with shape [numExcitationPatterns, 6].
        
        if calib is Gauss3D_Calibration, this functions returns a 3D estimator using 2D Astigmatic gaussian fit
        """            
        if type(calib) == gaussian.Gauss3D_Calibration:
            inst = self._SIMFLUX_Gauss2DAstig_CreateEstimator(num_patterns, calib, roisize, numframes, self.ctx.inst)
        else:
            if np.isscalar(calib):
                sigma_x, sigma_y = calib,calib
            else:
                sigma_x, sigma_y = calib
            
            inst = self._SIMFLUX_Gauss2D_CreateEstimator(num_patterns, sigma_x, sigma_y,
                                                      roisize, numframes, 
                                                      self.ctx.inst if self.ctx else None)
        return Estimator(self.ctx,inst)
        
        
    
    def DFT2D(self, xyI, k, useCuda=True):
        xyI = np.ascontiguousarray(xyI, dtype=np.float32)
        numpts = len(xyI)
        k = np.ascontiguousarray(k, dtype=np.float32)
        output = np.zeros( len(k), dtype=np.complex64)
        self._DFT2D(xyI, numpts, k, len(k), output, useCuda)
        return output

    # Convert an array of phases to an array of alternating XY modulation parameters
    def phase_to_mod(self, phases, omega, depth=1):
        mod = np.zeros((*phases.shape, 5), dtype=np.float32)
        mod[..., 0::2, 0] = omega  # kx
        mod[..., 1::2, 1] = omega  # ky
        mod[..., 2] = depth
        mod[..., 3] = phases
        mod[..., 4] = 1/len(mod)
        return mod

    