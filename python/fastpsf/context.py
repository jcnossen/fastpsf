# -*- coding: utf-8 -*-

import ctypes
from .lib import NativeLib

class Context:
    """
    Manage C++-side objects. 
    All native objects linked to the context will be destroyed when the context is destroyed.
    """
    def __init__(self, lib:NativeLib=None, debugMode=False):
        self.libOwner = lib is None
        if self.libOwner:
            lib = NativeLib(debugMode)

        self.lib = lib
        
        _lib = lib.lib
        self._Context_Create = _lib.Context_Create
        self._Context_Create.argtypes=[]
        self._Context_Create.restype = ctypes.c_void_p
        
        self._Context_Destroy = _lib.Context_Destroy
        self._Context_Destroy.argtypes = [ctypes.c_void_p]
        
        self.inst = self._Context_Create()

    def destroy(self):
        if self.inst:
            self._Context_Destroy(self.inst)
            self.inst=None
            
            if self.libOwner:
                self.lib.Close()
                self.lib = None
            
    def __enter__(self):
        return self
    
    def __del__(self):
        self.destroy()

    def __exit__(self, *args):
        self.destroy()


        