# -*- coding: utf-8 -*-

import ctypes
import os
import math
import numpy as np
import numpy.ctypeslib as ctl
import matplotlib.pyplot as plt
import sys

# https://stackoverflow.com/questions/32120178/how-can-i-pass-null-to-an-external-library-using-ctypes-with-an-argument-decla/32138619


def debugPrint(msg):
    sys.stdout.write(msg.decode("utf-8"))
    return 1 # also print using OutputDebugString on C++ side

#def debugImage(w,h,n,data,label):
#    sys.stdout.write(label.decode('utf-8'))

FloatArrayTypeBase = np.ctypeslib.ndpointer(dtype=np.float32, flags="C_CONTIGUOUS")

def _from_param_F32(cls, obj):
    if obj is None:
        return obj
    return FloatArrayTypeBase.from_param(obj)


NullableFloatArrayType = type("FloatArrayType", (FloatArrayTypeBase,), {"from_param": classmethod(_from_param_F32)})

IntArrayTypeBase = np.ctypeslib.ndpointer(dtype=np.int32, flags="C_CONTIGUOUS")

def _from_param_I32(cls, obj):
    if obj is None:
        return obj
    return IntArrayTypeBase.from_param(obj)

NullableIntArrayType = type("IntArrayType", (IntArrayTypeBase,), {"from_param": classmethod(_from_param_I32)})


class NativeLib:
    def __init__(self, debugMode=False):
        thispath = os.path.dirname(os.path.abspath(__file__))

        if ctypes.sizeof(ctypes.c_voidp) == 4:
            raise RuntimeError(f"The FastPSF library can only be used with 64-bit python.")

        if os.name == 'nt':
            name = 'fastpsf.dll'
        else:
            name = 'libfastpsf.so'

        if debugMode:
            dllpath = f"/../../bin/debug/{name}"
        else:
            dllpath = f"/../../bin/release/{name}"

        abs_dllpath = os.path.abspath(thispath + dllpath)
        
        #if not os.path.exists(abs_dllpath):
        #    abs_dllpath = pkg_resources.resource_filename('photonpy', 'data/')

        if debugMode:
            print("Using " + abs_dllpath)
        self.debugMode = debugMode
        
        currentDir = os.getcwd()
        os.chdir(os.path.dirname(abs_dllpath))

        lib = ctypes.CDLL(abs_dllpath)
        os.chdir(currentDir)
        
        self.lib_path = abs_dllpath
        self.lib = lib 
                
        #void CudaGetMemoryUse(int& pinnedBytes, int& devicePitchedBytes, int& deviceBytes, int& pitchedNumAllocs)        
        self.DebugPrintCallback = ctypes.CFUNCTYPE(ctypes.c_int32, ctypes.c_char_p)
        self._SetDebugPrintCallback = lib.SetDebugPrintCallback
        self._SetDebugPrintCallback.argtypes = [self.DebugPrintCallback]

#void(*cb)(int width,int height, int numImg, const float* data, const char* title));

        #
#CDLL_EXPORT void DrawROIs(float* images, int width, int height, const float* rois, 
#int numrois, int roisize, Int2* roiposYX, int* framenum)
        self._DrawROIs = lib.DrawROIs
        self._DrawROIs.argtypes = [
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"),
            ctypes.c_int32,
            ctypes.c_int32,
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"),
            ctypes.c_int32,
            ctypes.c_int32,
            ctl.ndpointer(np.int32, flags="aligned, c_contiguous"),
            ctl.ndpointer(np.int32, flags="aligned, c_contiguous")
        ]

        self.SetDebugPrintCallback(debugPrint)
        


    def SetDebugPrintCallback(self, fn):
        self.dbgPrintCallback = self.DebugPrintCallback(fn)  # make sure the callback doesnt get garbage collected
        self._SetDebugPrintCallback(self.dbgPrintCallback)

    def DrawROIs(self, imageOrShape, rois, roiposYX, framenum = None):
        if framenum is None:
            frame_ix = np.zeros(len(rois), dtype=np.int32)

            if len(np.array(imageOrShape).shape) == 1 and len(imageOrShape)==2:
                images = np.zeros(imageOrShape,dtype=np.float32)
            else:
                images = imageOrShape

        else:
            frame_ix = np.ascontiguousarray(framenum, dtype=np.int32)
            maxfr = framenum.max()+1
            images = imageOrShape
            assert len(images.shape) == 3
            assert maxfr <= len(images)

        if len(rois)>0:
            rois = np.ascontiguousarray(rois,dtype=np.float32)
            roiposYX = np.ascontiguousarray(roiposYX, dtype=np.int32)
            
            assert(len(rois.shape)==3)
            roisize = rois.shape[2]
            assert(rois.shape[1]==rois.shape[2])
            assert(np.array_equal(roiposYX.shape, (len(rois),2)))
            
            self._DrawROIs(images, images.shape[-1], images.shape[-2], rois, len(rois), roisize, roiposYX, frame_ix)
        return images

    def Close(self):
        if os.name == 'nt' and self.lib is not None:
            # Free DLL so we can overwrite the file when we recompile
            ctypes.windll.kernel32.FreeLibrary.argtypes = [ctypes.wintypes.HMODULE]
            ctypes.windll.kernel32.FreeLibrary(self.lib._handle)
            self.lib = None
        

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.Close()
        
