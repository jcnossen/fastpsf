For windows:

- Make sure Visual Studio 2019 and CUDA Toolkit 11.4 are installed (Could work with other versions too but not tested)
- Run build_windows.bat
- Open an anaconda prompt 
- cd \<repository root\>/python
- python setup.py develop
