// 2D Gaussian PSF models
// 
// photonpy - Single molecule localization microscopy library
// © Jelmer Cnossen 2018-2021
#include "DLLMacros.h"
#include "palala.h"
#include "Estimation.h"
#include "StringUtils.h"
#include "MathUtils.h"

#include "GaussianPSF.h"
#include "GaussianPSFModels.h"

#include "Estimator.h"
#include "EstimatorImpl.h"



template<typename TModel> 
Estimator* CreateGaussianPSF(int roisize, bool cuda, typename TModel::Calibration calib, 
	param_limits_t limits, int numconst, std::vector<float> stoplimit) 
{
	if (cuda) {
		auto* cuda_psf = new SimpleCalibrationPSF< cuPSFImpl< TModel, decltype(calib) > >
			(calib, { roisize,roisize }, limits, numconst, stoplimit);
		return new cuEstimatorWrapper(cuda_psf);
	}
	else {
		return new SimpleCalibrationPSF< PSFImpl< TModel, decltype(calib) > >
			(calib, { roisize,roisize }, limits, numconst, stoplimit);
	}
}

CDLL_EXPORT Estimator* Gauss2D_CreatePSF_XYZIBg(int roisize, const Gauss3D_Calibration& calib, bool cuda, Context* ctx)
{
	try {
		Estimator* psf;
		typedef Gauss2D_Model_XYZIBg Model;

		param_limits_t limits = {
			std::vector<float>{Gauss2D_Border, Gauss2D_Border, calib.minz, 10.0f, 1.0f},
			std::vector<float>{roisize - 1 - Gauss2D_Border, roisize - 1 - Gauss2D_Border, calib.maxz, 1e9f, 1e9f}
		};

		std::vector<float> stoplimit{ 1e-4f, 1e-4f, 1e-8f, 1e-4f, 1e-4f };

		psf = CreateGaussianPSF<Model>(roisize, cuda, calib, limits, Model::NumConstants, stoplimit);

		if (ctx) psf->SetContext(ctx);
		return psf;
	}
	catch (const std::runtime_error & e) {
		DebugPrintf("%s\n", e.what());
		return 0;
	}
}


CDLL_EXPORT Estimator* Gauss2D_CreatePSF_XYITiltedBg(int roisize, bool cuda, Context* ctx)
{
	try {
		Estimator* psf;

		param_limits_t limits = {
			std::vector<float>{Gauss2D_Border, Gauss2D_Border, 10.0f, 1.0f, -1e3f, -1e3f},
			std::vector<float>{roisize - 1 - Gauss2D_Border, roisize - 1 - Gauss2D_Border, 1e9f, 1e9f, 1e3f, 1e3f}
		};

		std::vector<float> stoplimit{ 1e-5f, 1e-5f, 1e-1f,1e-5f,1e-5f,1e-5f };

		psf = CreateGaussianPSF<Gauss2D_Model_XYITiltedBg>(roisize, cuda, {}, limits, Gauss2D_Model_XYITiltedBg::NumConstants, stoplimit);

		if (ctx) psf->SetContext(ctx);
		return psf;
	}
	catch (const std::runtime_error& e) {
		DebugPrintf("%s\n", e.what());
		return 0;
	}
}

CDLL_EXPORT Estimator* Gauss2D_CreatePSF_XYIBg(int roisize, float sigmaX, float sigmaY, bool cuda, Context* ctx)
{
	try {
		Estimator* psf;

		param_limits_t limits = {
			std::vector<float>{Gauss2D_Border, Gauss2D_Border, 10.0f, 1.0f},
			std::vector<float>{roisize - 1 - Gauss2D_Border, roisize - 1 - Gauss2D_Border, 1e9f, 1e9f}
		};

		std::vector<float> stoplimit{ 1e-5f, 1e-5f, 1e-1f,1e-5f };

		psf = CreateGaussianPSF<Gauss2D_Model_XYIBg>(roisize, cuda, { sigmaX,sigmaY }, limits, Gauss2D_Model_XYIBg::NumConstants,stoplimit);

		if (ctx) psf->SetContext(ctx);
		return psf;
	}
	catch (const std::runtime_error & e) {
		DebugPrintf("%s\n", e.what());
		return 0;
	}
}

CDLL_EXPORT Estimator* Gauss2D_CreatePSF_XYIBgConstSigma(int roisize, bool cuda, Context* ctx)
{
	try {
		Estimator* psf;

		param_limits_t limits = {
			std::vector<float>{Gauss2D_Border, Gauss2D_Border, 10.0f, 1.0f},
			std::vector<float>{roisize - 1 - Gauss2D_Border, roisize - 1 - Gauss2D_Border, 1e9f, 1e9f}
		};

		std::vector<float> stoplimit{ 1e-5f, 1e-5f, 1e-1f,1e-5f };

		psf = CreateGaussianPSF<Gauss2D_Model_XYIBgConstSigma>(roisize, cuda, {}, limits, 
			Gauss2D_Model_XYIBgConstSigma::NumConstants,stoplimit);

		if (ctx) psf->SetContext(ctx);
		return psf;
	}
	catch (const std::runtime_error& e) {
		DebugPrintf("%s\n", e.what());
		return 0;
	}
}



CDLL_EXPORT Estimator* Gauss2D_CreatePSF_XYIBgSigma(int roisize, float initialSigma, bool cuda, Context* ctx)
{
	try {
		Estimator* psf;

		param_limits_t limits = {
			std::vector<float>{Gauss2D_Border, Gauss2D_Border, 10.0f, 1.0f, Gauss2D_MinSigma},
			std::vector<float>{roisize-1-Gauss2D_Border, roisize - 1 - Gauss2D_Border, 1e9f, 1e9f,roisize * 2.0f}
		};
		std::vector<float> sl{ 1e-5f, 1e-5f, 1e-1f,1e-4f, 1e-5f };
		psf = CreateGaussianPSF<Gauss2D_Model_XYIBgSigma>(roisize, cuda, initialSigma, limits,
			Gauss2D_Model_XYIBgSigma::NumConstants,sl);

		if (ctx) psf->SetContext(ctx);
		return psf;
	}
	catch (const std::runtime_error & e) {
		DebugPrintf("%s\n", e.what());
		return 0;
	}
}



CDLL_EXPORT Estimator* Gauss2D_CreatePSF_XYIBgSigmaXY(int roisize, float initialSigmaX, float initialSigmaY, bool cuda, Context* ctx)
{
	try {
		Estimator* psf;
		Vector2f initialSigma = { initialSigmaX,initialSigmaY };

		param_limits_t limits = {
			std::vector<float>{Gauss2D_Border, Gauss2D_Border, 10.0f, 1.0f, Gauss2D_MinSigma,Gauss2D_MinSigma},
			std::vector<float>{roisize - 1 - Gauss2D_Border, roisize - 1 - Gauss2D_Border, 1e9f, 1e9f, roisize * 2.0f, roisize * 2.0f}
		};
		std::vector<float> sl{ 1e-5f, 1e-5f, 1e-1f,1e-4f,1e-5f,1e-5f };
		psf = CreateGaussianPSF<Gauss2D_Model_XYIBgSigmaXY>(roisize, cuda, initialSigma, limits, 0,sl);

		if (ctx) psf->SetContext(ctx);
		return psf;
	}
	catch (const std::runtime_error & e) {
		DebugPrintf("%s\n", e.what());
		return 0;
	}
}

CDLL_EXPORT Estimator* Gauss2D_CreatePSF_XYITiltedBgSigmaXY(int roisize, float initialSigmaX, float initialSigmaY, bool cuda, Context* ctx)
{
	try {
		Estimator* psf;
		Vector2f initialSigma = { initialSigmaX,initialSigmaY };

		param_limits_t limits = {
			std::vector<float>{Gauss2D_Border, Gauss2D_Border, 10.0f, 1.0f,-1e6f,-1e6f, Gauss2D_MinSigma,Gauss2D_MinSigma},
			std::vector<float>{roisize - 1 - Gauss2D_Border, roisize - 1 - Gauss2D_Border, 1e9f, 1e9f, 1e6f,1e6f, roisize * 2.0f, roisize * 2.0f}
		};
		std::vector<float> sl{ 1e-5f, 1e-5f, 1e-1f,1e-4f, 1e-4f,1e-4f,1e-5f,1e-5f };
		psf = CreateGaussianPSF<Gauss2D_Model_XYITiltedBgSigmaXY>(roisize, cuda, initialSigma, limits, 0,sl);

		if (ctx) psf->SetContext(ctx);
		return psf;
	}
	catch (const std::runtime_error& e) {
		DebugPrintf("%s\n", e.what());
		return 0;
	}
}



