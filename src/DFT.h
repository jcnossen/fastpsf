// photonpy - Single molecule localization microscopy library
// © Jelmer Cnossen 2018-2021
#pragma once

#include "DLLMacros.h"
#include "Vector.h"

CDLL_EXPORT void DFT2D(const Vector3f* xyI, int numpts, const Vector2f* k, int numk, Vector2f* output, bool useCuda);
