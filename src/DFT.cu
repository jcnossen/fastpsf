// SIMFLUX helper functions
// 
// photonpy - Single molecule localization microscopy library
// © Jelmer Cnossen 2018-2021
#include "palala.h"
#include "DFT.h"



CDLL_EXPORT void DFT2D(const Vector3f* xyI, int numpts, const Vector2f* k, int numk, Vector2f* output, bool useCuda)
{
	palala_for(numk, useCuda, PLL_FN(int i, const Vector3f* xyI, const Vector2f* k, Vector2f* output) {
		Vector2f k_ = k[i];

		// Use Kahan Sum for reduced errors
		Vector2f sum, c;

		for (int j = 0; j < numpts; j++) {
			float p = xyI[j][0] * k_[0] + xyI[j][1] * k_[1];
			float I = xyI[j][2];
			Vector2f input = { cos(p) * I, sin(p) * I };
			Vector2f y = input - c;
			Vector2f t = sum + y;
			c = (t - sum) - y;
			sum = t;
		}
		output[i] = sum;
	}, const_array(xyI, numpts),
		const_array(k, numk),
		out_array(output, numk));
}

